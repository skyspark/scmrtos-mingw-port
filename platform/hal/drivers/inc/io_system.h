/**
 * @file  io_system.h
 * @brief ���������� ��������� �����-������
 */

#ifndef IO_SYSTEM_H_
#define IO_SYSTEM_H_


/** ������� ������ ��������� ����������  */
#define SYSTICK_FREQ	1000

/** ���������� io ������� ������ BlueTooth */
#define IO_BT    1

/** ���������� io ������� GPS ������ */
#define IO_GPS   1

/** ���������� io ������� GSM ������ */
#define IO_GSM   1

/** ���������� io ������� ������� */
#define IO_SENS  1

/** ���������� io ������� �������������� */
#define IO_OXI   0

/** --------------------------------------------------------
 * ������� ������ ���������
 * @{
 */
#define PROC_SENS_STACK  	1024
#define PROC_GSM_STACK   	3000
#define PROC_SOLV_STACK	 	1024
#define PROC_GPS_STACK   	3000
#define PROC_OXI_STACK   	1024
#define PROC_SERCON_STACK	2048
/** @} */
/** ------------------------------------------------------*/
/// ��������� ���������
#include <process.h>
/** ------------------------------------------------------*/
/// ��������

/** ------------------------------------------------------*/

/** ------------------------------------------------------*/
#ifndef __IO_SYSTEM_CPP
#define IO_EXTERN extern
#else
#define IO_EXTERN
#endif

/** -----------------------------------------------------*/
#endif /* IO_SYSTEM_H_ */


INCLUDEPATH += platform/rtos/Common \
		platform/rtos/minGW \
		platform/rtos/include \
                 platform/rtos/Extensions \
               platform/hal/drivers\inc

HEADERS += \
    platform/platform.h \	
    platform/rtos/Common/OS_Kernel.h \
    platform/rtos/Common/OS_Services.h \
    platform/rtos/Common/scmRTOS.h \
    platform/rtos/Common/scmRTOS_310_compat.h \
    platform/rtos/Common/scmRTOS_defs.h \
    platform/rtos/Common/usrlib.h \
    platform/rtos/Extensions/Profiler/profiler.h \
    platform/rtos/include/process.h \
    platform/rtos/include/scmRTOS_CONFIG.h \
    platform/rtos/include/scmRTOS_extensions.h \
    platform/rtos/include/scmRTOS_TARGET_CFG.h \
    platform/rtos/minGW/OS_Target.h
    platform/hal/drivers/inc/io_system.h


SOURCES += \
    platform/platform.cpp \	
    platform/rtos/Common/OS_Kernel.cpp \
    platform/rtos/Common/OS_Services.cpp \
    platform/rtos/Common/usrlib.cpp \
    platform/rtos/minGW/OS_Target_cpp.cpp

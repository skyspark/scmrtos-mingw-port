#-------------------------------------------------
#
# Project created by QtCreator 2014-07-19T04:13:30
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lp-emulator
TEMPLATE = app

include(platform\platform.pri)
#include(terminal\terminal.pri)

INCLUDEPATH +=  platform

FORMS    += mainwindow.ui

CONFIG += mobility


MOBILITY = 

HEADERS += \
   mainwindow.h \


SOURCES += \
    main.cpp \
    mainwindow.cpp \


